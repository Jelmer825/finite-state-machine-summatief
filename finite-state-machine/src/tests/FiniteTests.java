package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javafx.util.Pair;
import org.junit.jupiter.api.Test;
import program.Finite;
import program.Node;

import java.util.LinkedList;

class FiniteTests {

    @Test
    void generateChars() {
        Finite finite = new Finite("BAABC");
        finite.generateChars("ABC");
        assertEquals(3, finite.getChars().size());
    }

    @Test
    void generateNodes() {
        Finite finite = new Finite("BAABC");
        finite.generateNodes(8);
        assertEquals(8, finite.getNodes().size());
    }

    @Test
    void getRightTransition() {
        Finite finite = new Finite("BAABC");

        String charString = "AB";
        Character correctChar = charString.charAt(0);
        Character incorrectChar = charString.charAt(1);

        LinkedList<Pair<Node, Character>> pairList = new LinkedList<>();

        Node node1 = new Node("s0");
        Node node2 = new Node("S1");

        pairList.add(new Pair<>(node1, correctChar));
        pairList.add(new Pair<>(node2, incorrectChar));

        assertEquals(node1, finite.getRightTransition(pairList, charString.charAt(0)));

    }
}