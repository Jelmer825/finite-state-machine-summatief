package tests;

import javafx.util.Pair;
import org.junit.jupiter.api.Test;
import program.Finite;
import program.Node;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {

    @Test
    void addTransitionToList() {
        LinkedList<Pair<Node, Character>> transitionListTest = new LinkedList<>();

        Node node0 = new Node("s0");
        Node node1 = new Node("s1");

        char character = 'A';

        transitionListTest.add(new Pair<>(node1, character));

        node0.addTransitionToList(new Pair<>(node1, character));

        assertEquals(transitionListTest, node0.getTransitionList());
    }
}