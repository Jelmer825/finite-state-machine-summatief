package program;

import javafx.util.Pair;

import java.util.*;

public class Node {

    private final String state;
    private final LinkedList<Pair<Node, Character>> transitionList = new LinkedList<>();

    public Node(String state){
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public LinkedList<Pair<Node, Character>> getTransitionList() {
        return transitionList;
    }

    public void addTransitionToList(Pair<Node, Character> node){
        this.transitionList.add(node);
    }
}
