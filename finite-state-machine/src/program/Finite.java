package program;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.Random;

public class Finite {

    private final String inputString;
    private final LinkedList<Node> nodes = new LinkedList<>();
    private final LinkedList<Character> chars = new LinkedList<>();

    public Finite(String inputString){
        this.inputString = inputString;
    }

    public void go(String chars, int nodeAmount, int transitionAmount){
        // Initialise
        this.generateChars(chars);
        this.generateNodes(nodeAmount);
        this.setNodeTransitions(transitionAmount);

        // Start
        System.out.println(this.readString());
    }

    public LinkedList<Node> getNodes() {
        return nodes;
    }

    public LinkedList<Character> getChars() {
        return chars;
    }

    public void generateChars(String chars){
        for(int i = 0; i < chars.length(); i++)
        {
            char c = chars.charAt(i);
            this.chars.add(c);
        }
    }
    public void generateNodes(int numberOfNodes){
        for(int i = 0; i < numberOfNodes; i++){
            String state = "s" + i;
            this.nodes.add(new Node(state));
        }
    }

    public void setNodeTransitions(int transitions){
        Random rand = new Random();
        for (Node node : this.nodes)
        {
            for(int i = 0; i < transitions; i++) {
                int randomNodeIndex = rand.nextInt(this.nodes.size());
                int randomCharIndex = rand.nextInt(this.chars.size());
                Node randomNode = this.nodes.get(randomNodeIndex);
                Character randomChar = this.chars.get(randomCharIndex);
                node.addTransitionToList(new Pair<>(randomNode, randomChar));
            }
        }
    }

    public LinkedList<String> readString() throws NullPointerException {
        LinkedList<String> pathList = new LinkedList<>();

        Node currentNode = this.getRandomStartNode();
        pathList.add(currentNode.getState());
        for (int i = 0; i < this.inputString.length(); i++){
            char c = this.inputString.charAt(i);
            LinkedList<Pair<Node, Character>> currentOptions = currentNode.getTransitionList();
            currentNode = this.getRightTransition(currentOptions, c);
            if (currentNode != null){
                pathList.add(currentNode.getState());
            } else{
                System.out.println(pathList);
                throw new NullPointerException("No transitions left! (stuck)");
            }
        }
        return pathList;
    }

    public Node getRightTransition(LinkedList<Pair<Node, Character>> pairList, Character c) {
        for (Pair<Node, Character> pair : pairList) {
            if (pair.getValue() == c){
                return pair.getKey();
            }
        }
        return null;
    }

    public Node getRandomStartNode(){
        Random rand = new Random();
        int randomIndex = rand.nextInt(this.nodes.size()-1);
        return this.nodes.get(randomIndex);
    }
}
